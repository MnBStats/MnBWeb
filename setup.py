from setuptools import setup, find_packages

setup(
    name='mnbstats',
    version='0.1.0.dev1',
    author="Piotr Wilk",
    license='MIT',
    packages=find_packages(exclude=['test*']),
    dependency_links=[
        'git+ssh://git@gitlab.mnbstats.com/MBStats/MnBModel.git'
    ],
    test_suite="tests"
)
