from mnbmodel import db
from mnbmodel.transactions.general_stats import general_stats
from mnbmodel.transactions import single_stats
from mnbweb.table import Table


class TourOverview:
    def __init__(self, general, hall_of_fame, eccentrics_corner):
        self.general_stats = general
        self.hall_of_fame = hall_of_fame
        self.eccentrics_corner = eccentrics_corner

    @classmethod
    def fetch(cls, database, tour_name):
        with db.session_scope(database) as session:
            limit = 5
            general = Table(
                title="General Stats",
                desc="Harman is great",
                headers=["Name", "Kills", "Deaths", "Teamkills", "Assists"],
                data=general_stats(session, tour_name, limit=10))

            hs = Table(
                title="Last Elves on Earth",
                desc="Top {} headshots.".format(limit),
                headers=["Name", "K"],
                data=single_stats.headshots(session, tour_name, limit=limit))

            cl = Table(
                title="Impalers",
                desc="Top {} couched lance kills.".format(limit),
                headers=["Name", "K"],
                data=single_stats.couches(session, tour_name, limit=limit))

            me = Table(
                title="Butchers",
                desc="Top {} 1h weapon kills.".format(limit),
                headers=["Name", "K"],
                data=single_stats.melee(session, tour_name, limit=limit))

            hm = Table(
                title="Hammer Timer",
                desc="Top {} hammer kills.".format(limit),
                headers=["Name", "K"],
                data=single_stats.hammer(session, tour_name, limit=limit))

            tks = Table(
                title="Berserkers",
                desc="Top {} teamkillers.".format(limit),
                headers=["Name", "TK"],
                data=single_stats.teamkillers(session, tour_name, limit=limit))

            tkd = Table(
                title="The Hexed",
                desc="Top {} most teamkilled players.".format(limit),
                headers=["Name", "D"],
                data=single_stats.teamkilled(session, tour_name, limit=limit))

            return TourOverview(
                general=general,
                hall_of_fame=[hs, cl, me],
                eccentrics_corner=[tks, tkd, hm])
