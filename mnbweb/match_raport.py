import collections
import pygal
from flask import Markup
from mnbmodel.query import read
from mnbmodel.transactions.player_classes import get_classes, RANGED_CLASSES,\
    INFANTRY_CLASSES, CAVALRY_CLASSES
from mnbmodel.transactions.deaths_order_chart import read_deaths_order_in_match
from mnbmodel.transactions.match_class_chart import read_kills_per_class
from mnbmodel.transactions import general_stats as g
from mnbweb.table import Table


def create_stats_table(session, match):
    MatchTable = collections.namedtuple('MatchTable', ['home', 'away'])

    home_table = _create_table(
        team_name=match.home_team.team_name,
        stats=team_stats(session, match, match.home_team))

    away_table = _create_table(
        team_name=match.away_team.team_name,
        stats=team_stats(session, match, match.away_team))

    return MatchTable(home_table, away_table)


def team_stats(session, match, team):
    stats = g.team_stats(session, match, team)
    for playerEntry in stats:
        playerEntry.items['PLD'] = read.num_of_rounds_played_in_match(
            session, match, playerEntry.name)
        classes = get_classes(session, playerEntry.name, match)
        playerEntry.items['CLS'] = Markup('')
        playerEntry.items['CLS'] += _add_markup(
            classes, INFANTRY_CLASSES, 'infantry')
        playerEntry.items['CLS'] += _add_markup(
            classes, RANGED_CLASSES, 'ranged')
        playerEntry.items['CLS'] += _add_markup(
            classes, CAVALRY_CLASSES, 'cavalry')

    return stats


def _add_markup(classes, class_types, filename):
    return Markup("<img src='/static/{}.png'/>".format(
        filename if not classes.isdisjoint(class_types) else 'default'))


def _create_table(team_name, stats):
    return Table(
        title=team_name,
        desc="",
        headers=["Name", "K", "D", "TK", "A", "PLD", "CLS"],
        data=stats)


def create_death_order_chart(session, match, team):
    line_chart = pygal.Line(
        human_readable=True,
        legend_at_bottom=True,
        legend_at_bottom_columns=8)

    line_chart.title = '{} death order with death info.'.format(
        team.team_name,
        match.home_team.team_name,
        match.away_team.team_name)

    line_chart.x_labels = [
        'S{}R{}'.format(r.spawn, r.round)
        for r in read.rounds(session, match)]

    max_deaths_in_round = read.num_of_deaths_in_round(session, match) + 1
    line_chart.y_labels = [str(num) for num in range(1, max_deaths_in_round)]
    line_chart.y_labels.append('Survived')

    deaths = read_deaths_order_in_match(session, match, team, max_deaths_in_round)
    for name, death_info in deaths.items():
        data = [_create_label(d) for d in death_info]
        line_chart.add(name, data, allow_interruptions=True)

    return line_chart.render_data_uri()


def _create_label(death_info):
    label = ['Class: {}'.format(death_info.cls)]
    if death_info.weapon is not None:
        label.append(
            ', EnemyClass: {}, Weapon: {}'
             .format(death_info.enemy_cls, death_info.weapon))

    return {'value': death_info.death_number, 'label': ''.join(label)}


def create_class_chart(session, match, team):
    pie_chart = pygal.Pie()
    pie_chart.title = '{} kills per classes.'.format(team.team_name)

    kills = read_kills_per_class(session, match, team)
    pie_chart.add('Cavalry', [_create_weapons_label(k)for k in kills.cavalry])
    pie_chart.add('Infantry', [_create_weapons_label(k)for k in kills.infantry])
    pie_chart.add('Ranged', [_create_weapons_label(k)for k in kills.ranged])

    return pie_chart.render_data_uri()


def _create_weapons_label(data):
    return {'value': data[1], 'label': data[0]}


def create(session, match):
    MatchReport = collections.namedtuple(
        'MatchReport',
        ['stats_table',
         'home_class_chart', 'away_class_chart',
         'home_death_order_chart', 'away_death_order_chart'])

    return MatchReport(
        create_stats_table(session, match),
        create_class_chart(session, match, match.home_team),
        create_class_chart(session, match, match.away_team),
        create_death_order_chart(session, match, match.home_team),
        create_death_order_chart(session, match, match.away_team))
