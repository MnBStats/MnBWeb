import collections
from mnbmodel import db
from mnbmodel.query import read
from mnbmodel.transactions import single_stats
from mnbmodel.transactions import general_stats as g
from mnbweb.table import Table

StarOfTheWeek = collections.namedtuple('StarOfTheWeek', ['name', 'kills'])
HallOfFame = collections.namedtuple(
    'HallOfFame', ['headshots', 'melee', 'couches'])
MatchInfo = collections.namedtuple(
    'MatchInfo', ['home', 'away', 'home_score', 'away_score'])


class WeekReport:
    def __init__(self, matches, star, hof):
        self.matches = matches
        self.star = star
        self.hall_of_fame = hof

    @classmethod
    def fetch(cls, database, tour_name, week_number):
        return cls(
            matches=fetch_matches(database, tour_name, week_number),
            star=fetch_star(database, tour_name, week_number),
            hof=fetch_hall_of_fame(database, tour_name, week_number))


def fetch_star(database, tour_name, week_number):
    with db.session_scope(database) as session:
        return g.star_of_the_week(session, tour_name, week_number)


def fetch_matches(database, tour_name, week_number):
    matches = []
    with db.session_scope(database) as session:
        for match in read.matches(session, tour_name, week_number):
            matches.append(MatchInfo(
                home=match.home_team.team_name,
                away=match.away_team.team_name,
                home_score=match.home_team_score,
                away_score=match.away_team_score))

    return matches


def fetch_hall_of_fame(database, tour_name, week_number):
    limit = 5
    with db.session_scope(database) as session:
        hs = single_stats.headshots(
            session,
            tour_name,
            limit=limit,
            filters=[g.week_filter(week_number)])

        melee = single_stats.melee(
            session,
            tour_name,
            limit=limit,
            filters=[g.week_filter(week_number)])

        couches = single_stats.couches(
            session,
            tour_name,
            limit=limit,
            filters=[g.week_filter(week_number)])

        return HallOfFame(
            headshots=Table(
                title="Headshots",
                desc="This week's top {} headshots.".format(limit),
                headers=["Name", "K"],
                data=hs),
            melee=Table(
                title="Melee Kills",
                desc="This week's top {} 1h weapon kills.".format(limit),
                headers=["Name", "K"],
                data=melee),
            couches=Table(
                title="Couched Lances",
                desc="This week's top {} couched lance kills.".format(limit),
                headers=["Name", "K"],
                data=couches))
