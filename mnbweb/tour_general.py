from mnbmodel import db
from mnbmodel.transactions.general_stats import general_stats
from mnbweb.table import Table


def fetch(database, tour_name):
    with db.session_scope(database) as session:
        return Table(
            title="General Stats",
            desc="Harman is great",
            headers=["Name", "Kills", "Deaths", "Teamkills", "Assists"],
            data=general_stats(session, tour_name))
