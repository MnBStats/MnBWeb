from flask import Flask, render_template
from mnbmodel import db
from mnbmodel.query import read
from mnbweb import tour_overview, tour_general, match_stats, match_raport

app = Flask(__name__)
database = db.Database(conn_string='sqlite:///s_app.db')


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/tournaments')
def tournaments():
    with db.session_scope(database) as session:
        tours = read.tournaments(session)

        return render_template('tours.html', tournaments=tours)


@app.route('/<string:tournament_name>/overview')
def tournament(tournament_name):
    with db.session_scope(database) as session:
        tour = read.tournament(session, tournament_name)
        num_of_weeks = read.num_of_weeks(session, tournament_name)

        return render_template(
            'tour_overview.html',
            tour=tour,
            num_of_weeks=num_of_weeks,
            overview_data=tour_overview.TourOverview.fetch(
                database, tournament_name))


@app.route('/<string:tournament_name>/general')
def tour_general_stats(tournament_name):
    with db.session_scope(database) as session:
        tour = read.tournament(session, tournament_name)
        num_of_weeks = read.num_of_weeks(session, tournament_name)

        return render_template(
            'tour_general.html',
            tour=tour,
            num_of_weeks=num_of_weeks,
            general_stats=tour_general.fetch(database, tournament_name))


@app.route('/<string:tournament_name>/<int:week_number>')
def weekly_stats(tournament_name, week_number):
    with db.session_scope(database) as session:
        tour = read.tournament(session, tournament_name)
        num_of_weeks = read.num_of_weeks(session, tournament_name)

        return render_template(
            'tour_week.html',
            tour=tour,
            num_of_weeks=num_of_weeks,
            week_number=week_number,
            stats=match_stats.WeekReport.fetch(
                database, tournament_name, week_number))


@app.route(
    '/<string:tournament_name>/<int:week_number>/<string:team_name>')
def match_page(tournament_name, week_number, team_name):
    with db.session_scope(database) as session:
        tour = read.tournament(session, tournament_name)
        num_of_weeks = read.num_of_weeks(session, tournament_name)
        team = read.team_from_name(session, team_name, tour)
        match = read.match(session, tour, week_number, team)

        return render_template(
            'secret.html',
            tour=tour,
            num_of_weeks=num_of_weeks,
            week_number=week_number,
            match_report=match_raport.create(session, match))


if __name__ == '__main__':
    app.run(debug=True)
